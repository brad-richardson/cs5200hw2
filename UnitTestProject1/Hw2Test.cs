﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using cs5200hw2;
using Messages;
using SharedObjects;
using System.Configuration;

namespace UnitTestProject1
{
    [TestClass]
    public class Hw2Test
    {
        [TestMethod]
        public void ConversationCreation()
        {
            Conversation conv = Conversation.CreateConversation(new AliveRequest());
            Assert.AreEqual(conv.GetType(), new AliveConversation().GetType());
            Assert.IsNull(conv.Comm);
            Assert.IsNull(conv.Queue);

            conv = Conversation.CreateConversation(new LoginReply());
            Assert.AreEqual(conv.GetType(), new LoginConversation().GetType());

            conv = Conversation.CreateConversation(new GameListReply());
            Assert.AreEqual(conv.GetType(), new GameListConversation().GetType());

            conv = Conversation.CreateConversation(new JoinGameReply());
            Assert.AreEqual(conv.GetType(), new JoinGameConversation().GetType());
        }

        [TestMethod]
        public void AliveConversationTest()
        {
            State state = State.Instance;
            string currAlive = state.LastAliveRequest;

            AliveRequest req = new AliveRequest();
            Envelope env = new Envelope();
            env.message = req;

            Conversation conv = Conversation.CreateConversation(req);
            conv.Process(env);

            Assert.AreNotEqual(currAlive, state.LastAliveRequest);
        }

        [TestMethod]
        public void GameListConversationTest()
        {
            State state = State.Instance;
            GameInfo[] gameInfo = new GameInfo[1];
            gameInfo[0] = new GameInfo();
            gameInfo[0].GameId = 1;

            GameListReply reply = new GameListReply();
            reply.GameInfo = gameInfo;
            Envelope env = new Envelope();
            env.message = reply;

            Conversation conv = Conversation.CreateConversation(reply);
            conv.Process(env);

            Assert.AreEqual(state.GameInfo, gameInfo);
        }

        [TestMethod]
        public void JoinGameConversationTest()
        {
            State state = State.Instance;
            JoinGameReply reply = new JoinGameReply();
            reply.GameId = 12;
            
            Envelope env = new Envelope();
            env.message = reply;

            Conversation conv = Conversation.CreateConversation(reply);
            conv.Process(env);

            Assert.AreEqual(state.JoinedGameId, 12);
        }

        [TestMethod]
        public void LoginConversationTest()
        {
            State state = State.Instance;
            LoginReply reply = new LoginReply();
            reply.Note = "Logged in";
            ProcessInfo pInfo = new ProcessInfo();
            reply.ProcessInfo = pInfo;

            Envelope env = new Envelope();
            env.message = reply;

            Conversation conv = Conversation.CreateConversation(reply);
            conv.Process(env);

            Assert.AreEqual(state.ProcessInfo, pInfo);
            Assert.AreEqual(state.Status, "Logged in");
        }

        [TestMethod]
        public void PlayerTest()
        {
            Player player = new Player();
            LoginRequest req = player.CreateLoginRequest();
            Assert.AreEqual(req.ProcessLabel, "brad's player");
            Assert.AreEqual(req.ProcessType, ProcessInfo.ProcessType.Player);
        }
    }
}
