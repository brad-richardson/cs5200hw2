﻿using cs5200hw2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestProject1
{
    public class MockListener : Listener
    {
        public Envelope lastEnvelope { get; set; }

        public override void ProcessEnvelope(Envelope envelope)
        {
            lastEnvelope = envelope;
        }
    }
}
