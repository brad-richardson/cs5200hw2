﻿using cs5200hw2;
using Messages;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestProject1
{
    [TestClass]
    public class EnvelopeTest
    {
        [TestMethod]
        public void EnvelopeCreation()
        {
            Envelope env = new Envelope();

            Assert.IsNull(env.endPoint);
            Assert.IsNull(env.message);

            IPEndPoint ep = new IPEndPoint(IPAddress.Any, 0);
            env.endPoint = ep;

            Message message = new Message();
            env.message = message;

            Assert.AreEqual(ep, env.endPoint);
            Assert.AreEqual(message, env.message);
        }
    }
}
