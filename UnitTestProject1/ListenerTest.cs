﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using cs5200hw2;
using SharedObjects;
using Messages;

namespace UnitTestProject1
{
    [TestClass]
    public class ListenerTest
    {
        [TestMethod]
        public void ListenerTester()
        {
            Listener listener = new ListenerImpl();

            Envelope env = new Envelope();
            Message message = new Message();
            MessageNumber.ResetSeqNumber();
            message.ConversationId = MessageNumber.Create();

            env.message = message;

            listener.ProcessEnvelope(env);

            QueueDictionary dictionary = QueueDictionary.Instance;
            EnvelopeQueue queue;
            if (dictionary.TryGetValue(QueueDictionary.uncreatedMessageNumber, out queue))
            {
                Assert.IsNotNull(queue.Take());
            }
        }
    }
}
