﻿using cs5200hw2;
using Messages;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SharedObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UnitTestProject1
{
    [TestClass]
    public class CommunicatorTest
    {
        [TestMethod]
        public void CommunicatorCreation()
        {
            Communicator comm = new Communicator();
            Assert.IsNotNull(comm.udpClient);
            Assert.IsNotNull(comm.registryEndPoint);
        }

        [TestMethod]
        public void CommunicatorCommunication()
        {
            Communicator comm1 = new Communicator();
            comm1.startListening();
            IPEndPoint comm1Ep = comm1.udpClient.Client.LocalEndPoint as IPEndPoint;
            MockListener list1 = new MockListener();
            comm1.listener = list1;

            Communicator comm2 = new Communicator();
            comm2.startListening();
            IPEndPoint comm2Ep = comm2.udpClient.Client.LocalEndPoint as IPEndPoint;
            MockListener list2 = new MockListener();
            comm2.listener = list2;

            comm1.registryEndPoint = comm2Ep;
            comm1.registryEndPoint.Address = IPAddress.Parse("127.0.0.1");
            comm2.registryEndPoint = comm1Ep;
            comm2.registryEndPoint.Address = IPAddress.Parse("127.0.0.1");

            MessageNumber.ResetSeqNumber();
            Message msg1 = new Message();
            msg1.InitMessageAndConversationNumbers();
            Message msg2 = new Message();
            msg2.InitMessageAndConversationNumbers();

            comm1.SendMessageToRegistry(msg1);
            comm2.SendMessageToRegistry(msg2);

            Thread.Sleep(100);
            
            Assert.AreEqual(list1.lastEnvelope.message.ConversationId, msg2.ConversationId);
            Assert.AreEqual(list2.lastEnvelope.message.ConversationId, msg1.ConversationId);
        }

        [TestMethod]
        public void TestNullEnv()
        {
            Communicator comm1 = new Communicator();
            comm1.startListening();
            comm1.SendEnvelope(null);
            Envelope env = new Envelope();
            comm1.SendEnvelope(env);
            env.endPoint = comm1.udpClient.Client.LocalEndPoint as IPEndPoint;
            env.endPoint.Address = IPAddress.Parse("127.0.0.1");
            comm1.SendEnvelope(env);
            env.message = new Message();
            comm1.SendEnvelope(env);
        }
    }
}
