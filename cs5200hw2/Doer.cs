﻿using log4net;
using Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace cs5200hw2
{
    public class Doer
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Doer));
        QueueDictionary queueDictionary;
        Communicator Comm;
        private Thread doerThread = null;

        public Doer(Communicator comm)
        {
            Comm = comm;
        }

        public void Start()
        {
            queueDictionary = QueueDictionary.Instance;
            doerThread = new Thread(new ThreadStart(Run));
            doerThread.Start();
        }

        private void Run()
        {
            EnvelopeQueue defaultQueue;
            queueDictionary.TryGetValue(QueueDictionary.uncreatedMessageNumber, out defaultQueue);

            while(true)
            {
                Envelope envelope = defaultQueue.Take();
                ProcessMessage(envelope);
            }
        }

        private void ProcessMessage(Envelope env)
        {
            EnvelopeQueue queue = new EnvelopeQueue();
            queue.Add(env);
            queueDictionary.Add(env.message.ConversationId, queue);

            Conversation conversation = Conversation.CreateConversation(env.message);
            conversation.Queue = queue;
            conversation.Comm = Comm;
            conversation.Start();
        }
    }
}
