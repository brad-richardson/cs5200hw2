﻿using Messages;
using SharedObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs5200hw2
{
    public class LoginConversation : Conversation
    {

        public override void Process(Envelope env)
        {
            LoginReply reply = (LoginReply) env.message;

            state.ProcessInfo = reply.ProcessInfo;
            MessageNumber.LocalProcessId = state.ProcessInfo.ProcessId;
            state.Status = reply.Note;

            GetGameList();
        }

        private void GetGameList()
        {
            GameListRequest gameListRequest = new GameListRequest();
            gameListRequest.StatusFilter = (int)GameInfo.StatusCode.Available;
            gameListRequest.InitMessageAndConversationNumbers();
            if (Comm != null)
            {
                Comm.SendMessageToRegistry(gameListRequest);
            }
        }
    }
}
