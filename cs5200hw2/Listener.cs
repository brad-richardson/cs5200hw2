﻿using log4net;
using Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs5200hw2
{
    public abstract class Listener
    {
        public abstract void ProcessEnvelope(Envelope envelope);
    }
}
