﻿using Messages;
using SharedObjects;
using System;

namespace cs5200hw2
{
    public class GameListConversation : Conversation
    {

        public override void Process(Envelope env)
        {
            GameListReply reply = (GameListReply) env.message;
            state.GameInfo = reply.GameInfo;
            if (state.GameInfo.Length > 0)
            {
                JoinAnyGame();
            }
        }

        private void JoinAnyGame()
        {
            GameInfo game = state.GameInfo[0];

            JoinGameRequest joinGameRequest = new JoinGameRequest();
            joinGameRequest.Player = state.ProcessInfo;
            joinGameRequest.GameId = game.GameId;
            joinGameRequest.InitMessageAndConversationNumbers();
            
            if (Comm != null)
            {
                Envelope env = new Envelope();
                env.message = joinGameRequest;
                env.endPoint = game.GameManager.EndPoint.IPEndPoint;
                Comm.SendEnvelope(env);
            }
        }
    }
}