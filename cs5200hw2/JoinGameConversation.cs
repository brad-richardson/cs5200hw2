﻿using Messages;
using System;

namespace cs5200hw2
{
    public class JoinGameConversation : Conversation
    {

        public override void Process(Envelope env)
        {
            JoinGameReply response = (JoinGameReply)env.message;
            state.JoinedGameId = response.GameId;
        }
    }
}