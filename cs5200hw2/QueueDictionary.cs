﻿using SharedObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs5200hw2
{
    public class QueueDictionary : Dictionary<MessageNumber,EnvelopeQueue>
    {
        public static MessageNumber uncreatedMessageNumber;
        private static QueueDictionary instance;

        private QueueDictionary() { }

        public static QueueDictionary Instance
        {
            get
            {
                if(instance == null)
                {
                    instance = new QueueDictionary();

                    uncreatedMessageNumber = new MessageNumber();
                    uncreatedMessageNumber.ProcessId = -1;
                    uncreatedMessageNumber.SeqNumber = -1;
                    instance.Add(uncreatedMessageNumber, new EnvelopeQueue());
                }
                return instance;
            }
        }
    }
}
