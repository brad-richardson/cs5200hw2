﻿using Messages;
using SharedObjects;
using System;

namespace cs5200hw2
{
    public class AliveConversation : Conversation
    {

        public override void Process(Envelope env)
        {
            SendReply(env.message);
            state.LastAliveRequest = DateTime.Now.ToShortTimeString();
        }

        private void SendReply(Message msg)
        {
            Reply reply = new Reply();
            reply.SetMessageAndConversationNumbers(MessageNumber.Create(), msg.ConversationId);
            if (Comm != null)
            {
                Comm.SendMessageToRegistry(reply);
            }
        }
    }
}