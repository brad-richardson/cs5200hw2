﻿using Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace cs5200hw2
{
    public abstract class Conversation
    {
        public Communicator Comm { get; set; }
        public EnvelopeQueue Queue { get; set; }
        protected State state;

        public Conversation()
        {
            state = State.Instance;
        }

        public void Start()
        {
            Thread listener = new Thread(new ThreadStart(Listen));
            listener.Start();
        }

        private void Listen()
        {
            while (true)
            {
                Envelope env = Queue.Take();
                Process(env);
            }
        }

        public abstract void Process(Envelope env);

        public static Conversation CreateConversation(Message msg)
        {
            if (typeof(LoginReply).Equals(msg.GetType()))
            {
                return new LoginConversation();
            }
            else if (typeof(AliveRequest).Equals(msg.GetType()))
            {
                return new AliveConversation();
            }
            else if (typeof(GameListReply).Equals(msg.GetType()))
            {
                return new GameListConversation();
            }
            else if (typeof(JoinGameReply).Equals(msg.GetType()))
            {
                return new JoinGameConversation();
            }
            else
            {
                throw new NotImplementedException();
            }
        }
    }
}
