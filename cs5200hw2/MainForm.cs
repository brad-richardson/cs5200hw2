﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;

namespace cs5200hw2
{
    public partial class MainForm : Form
    {
        Player player;
        State state;

        public MainForm()
        {
            InitializeComponent();

            player = new Player();
            player.StartPlaying();

            this.state = State.Instance;
            state.PropertyChanged += PropertyChanged;

            process_label_text.Text = ConfigurationManager.AppSettings["ProcessLabel"];
            registry_ep_text.Text = ConfigurationManager.AppSettings["IPAddress"]
                + ":" + ConfigurationManager.AppSettings["Port"];
        }

        private void PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            Invoke((MethodInvoker)delegate
            {
                status_label.Text = state.Status;
                if (state.JoinedGameId != -1)
                {
                    gamejoin_label.Text = state.JoinedGameId.ToString();
                }
                label1.Text = state.LastAliveRequest;
            });
        }
    }
}
