﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharedObjects;
using Messages;
using log4net;
using System.Net.Sockets;
using System.Net;
using System.Configuration;

namespace cs5200hw2
{
    public class Communicator
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Communicator));
        public IPEndPoint registryEndPoint { get; set; }
        public UdpClient udpClient { get; set; }
        public Listener listener { get; set; }

        public Communicator()
        {
            string remoteLocation = ConfigurationManager.AppSettings["IPAddress"]
                + ":" + ConfigurationManager.AppSettings["Port"];
            registryEndPoint = new PublicEndPoint(remoteLocation).IPEndPoint;

            udpClient = new UdpClient(new IPEndPoint(IPAddress.Any, 0));
            listener = new ListenerImpl();
        }

        public void startListening()
        {
            Task.Run(async () =>
            {
                using (udpClient)
                {
                    while (true)
                    {
                        UdpReceiveResult result = await udpClient.ReceiveAsync();
                        ResultToListener(result);
                    }
                }
            });
        }

        private void ResultToListener(UdpReceiveResult result)
        {
            Envelope envelope = new Envelope();
            envelope.endPoint = result.RemoteEndPoint;
            envelope.message = Message.Decode(result.Buffer);

            log.Debug(Encoding.ASCII.GetString(result.Buffer));

            listener.ProcessEnvelope(envelope);
        }

        public void SendEnvelope(Envelope env)
        {
            if(env == null)
            {
                log.Error("Received null envelope!");
                return;
            }
            else if (env.endPoint == null)
            {
                log.Error("No end point to send message!");
                return;
            }
            else if (env.message == null)
            {
                log.Error("No message to send!");
                return;
            }
            byte[] sendBytes = env.message.Encode();

            log.Debug(Encoding.ASCII.GetString(sendBytes));

            udpClient.Send(sendBytes, sendBytes.Length, env.endPoint);
        }

        public void SendMessageToRegistry(Message message)
        {
            Envelope env = new Envelope();
            env.message = message;
            env.endPoint = registryEndPoint;

            SendEnvelope(env);
        }
    }
}
