﻿using SharedObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace cs5200hw2
{
    public class State : INotifyPropertyChanged
    {
        private static State instance;

        private string status;
        public string Status { get { return status; }
            set
            {   
                if(value != status)
                {
                    status = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private ProcessInfo processInfo;
        public ProcessInfo ProcessInfo { get { return processInfo; }
            set
            {
                if (value != processInfo)
                {
                    processInfo = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private GameInfo[] gameInfo;
        public GameInfo[] GameInfo
        {
            get { return gameInfo; }
            set
            {
                if (value != gameInfo)
                {
                    gameInfo = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private int joinedGameId;
        public int JoinedGameId
        {
            get { return joinedGameId; }
            set
            {
                if (value != joinedGameId)
                {
                    joinedGameId = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string lastAliveRequest;
        public string LastAliveRequest
        {
            get { return lastAliveRequest; }
            set
            {
                if (value != lastAliveRequest)
                {
                    lastAliveRequest = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public static State Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new State();

                    instance.status = "";
                    instance.joinedGameId = -1;
                    instance.lastAliveRequest = "";
                }
                return instance;
            }
        }
    }
}
