﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs5200hw2
{
    public class ListenerImpl : Listener
    {
        public override void ProcessEnvelope(Envelope envelope)
        {
            QueueDictionary dictionary = QueueDictionary.Instance;
            EnvelopeQueue queue;
            if (dictionary.TryGetValue(envelope.message.ConversationId, out queue))
            {
                queue.Add(envelope);
            }
            else if (dictionary.TryGetValue(QueueDictionary.uncreatedMessageNumber, out queue))
            {
                queue.Add(envelope);
            }
        }
    }
}
