﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Messages;
using SharedObjects;
using System.Net;

namespace cs5200hw2
{
    public class Envelope
    {
        public Message message { get; set; }
        public IPEndPoint endPoint { get; set; }
    }
}
