﻿using Messages;
using SharedObjects;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace cs5200hw2
{
    public class Player
    {
        private Doer Doer;
        private Communicator Comm;

        public Player()
        {
            Comm = new Communicator();
            Comm.startListening();

            Doer = new Doer(Comm);
        }

        public void StartPlaying()
        {
            Login();
            Doer.Start();
        }

        private void Login()
        {
            Comm.SendMessageToRegistry(CreateLoginRequest());
        }

        public LoginRequest CreateLoginRequest()
        {
            IdentityInfo identity = new IdentityInfo();
            identity.FirstName = ConfigurationManager.AppSettings["FirstName"];
            identity.LastName = ConfigurationManager.AppSettings["LastName"];
            identity.ANumber = ConfigurationManager.AppSettings["ANumber"];
            identity.Alias = ConfigurationManager.AppSettings["Alias"];

            LoginRequest loginRequest = new LoginRequest();
            loginRequest.Identity = identity;
            loginRequest.ProcessLabel = ConfigurationManager.AppSettings["ProcessLabel"];
            loginRequest.ProcessType = ProcessInfo.ProcessType.Player;
            loginRequest.InitMessageAndConversationNumbers();

            return loginRequest;
        }

    }
}
